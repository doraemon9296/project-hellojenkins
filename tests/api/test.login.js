const assert = require('assert');
const request = require('supertest');
const server = require("../../index");
const agent = request(server);

describe('Test Login API', function () {
    let path = "/login";

    let testValidUser = {
        username: 'validuser',
        password: 'strongpassword'
    }

    let testInvalidUser = {
        username: 'invaliduser',
        password: 'wrongpassword'
    }

    it('[驗證身份] 合法使用者，須回傳 200', function(done) {
        agent
            .post(path)
            .send(testValidUser)
            .expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                done();
            })
    });

    it('[驗證身份] 非法使用者，須回傳 401', function(done) {
        agent
            .post(path)
            .send(testInvalidUser)
            .expect(401)
            .end(function (err, res) {
                if (err) return done(err);
                done();
            })
    });
})